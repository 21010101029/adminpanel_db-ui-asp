﻿using AdminPanel_UI.DAL.LOC_CityDAL;
using Microsoft.AspNetCore.Mvc;
using System.Data;

namespace AdminPanel_UI.Areas.City.Controllers
{
    [Area("City")]
    [Route("City/[controller]/[action]")]
    public class LOC_CityController : Controller
    {
        LOC_CityDAL dalLOC_CityDAL = new LOC_CityDAL();

        #region LOC_CityList Controller
        public IActionResult LOC_CityList()
        {
            DataTable dt= dalLOC_CityDAL.dbo_PR_LOC_City_SelectAll();
            return View(dt);
        }
        #endregion

        public IActionResult Add()
        {
            return View("AddEditCity");
        }
        public IActionResult AddEditCity()
        {
            return View();
        }
    }
}
