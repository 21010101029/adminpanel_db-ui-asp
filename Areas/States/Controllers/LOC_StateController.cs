﻿using AdminPanel_UI.DAL.LOC_StateDAL;
using Microsoft.AspNetCore.Mvc;
using System.Data;

namespace AdminPanel_UI.Areas.States.Controllers
{
    [Area("States")]
    [Route("States/[controller]/[action]")]
    public class LOC_StateController : Controller
    {
        LOC_StateDAL dalLOC_StateDAL = new LOC_StateDAL();

        #region LOC_StateList
        public IActionResult LOC_StateList()
        {
            DataTable dt = dalLOC_StateDAL.dbo_PR_LOC_State_SelectAll();
            return View(dt);
        }
        #endregion
        public IActionResult Add()
        {
            return View("AddEditState");
        }
        public IActionResult AddEditState()
        {
            return View();
        }
    }
}
