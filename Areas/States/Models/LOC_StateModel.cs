﻿using System.ComponentModel.DataAnnotations;

namespace AdminPanel_UI.Areas.States.Models
{
    public class LOC_StateModel
    {
        public int StateID { get; set; }    
        public string StateName { get; set; } = string.Empty;
        public string StateCode { get; set; }
        public string CountryName { get; set; }
        [Required]
        public string CountryCode { get; set; }

        public DateTime? Created { get; set; }
        public DateTime? Modified { get; set; }
    }
}
