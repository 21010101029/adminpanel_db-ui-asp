﻿using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data;
using System.Data.Common;

namespace AdminPanel_UI.DAL.LOC_StateDAL
{
    public class LOC_StateDALBase:DALHelper
    {
        #region dbo_PR_LOC_State_SelectAll
        public DataTable dbo_PR_LOC_State_SelectAll()
        {
            try
            {
                SqlDatabase sqlDatabase = new SqlDatabase(myConnectionString);
                DbCommand dbCommand = sqlDatabase.GetStoredProcCommand("PR_LOC_State_SelectAll");
                DataTable dt = new DataTable(); 
                using (IDataReader reader = sqlDatabase.ExecuteReader(dbCommand))
                {
                    dt.Load(reader);
                }
                return dt;
            }catch (Exception ex)
            {
                return null;
            }
        }
        #endregion
    }
}
